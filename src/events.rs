/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use sdl2::event::Event::KeyDown;
use sdl2::event::Event::KeyUp;
use sdl2::event::Event::MouseMotion;
use sdl2::event::Event::MouseButtonDown;
use sdl2::event::Event::MouseButtonUp;
use sdl2::keyboard::Scancode;
use sdl2::mouse::MouseButton; // the enum of buttons
use sdl2::event::Event;

use std::collections::HashMap;
use std::collections::HashSet;

pub struct Events {
    pump: ::sdl2::EventPump,
    pub actions: HashSet<String>,
    bindings: Bindings,
    pub mouse_x: i32,
    pub mouse_y: i32,
}
struct Bindings {
    k_bind: HashMap<Scancode, String>,
    m_bind: HashMap<MouseButton, String>,
}

impl Events {
    pub fn new(pump: ::sdl2::EventPump) -> Events {
        // Set up some defaults
        let mut mouse = HashMap::new();
        mouse.insert(MouseButton::Left, "insert_cell".to_string());
        let mut keys = HashMap::new();
        keys.insert(Scancode::P, "pause".to_string());
        keys.insert(Scancode::A, "pan_left".to_string());
        keys.insert(Scancode::W, "pan_up".to_string());
        keys.insert(Scancode::D, "pan_right".to_string());
        keys.insert(Scancode::S, "pan_down".to_string());
        keys.insert(Scancode::E, "zoom_in".to_string());
        keys.insert(Scancode::Q, "zoom_out".to_string());
        keys.insert(Scancode::Escape, "quit".to_string());
        keys.insert(Scancode::Num1, "pattern1".to_string());
        keys.insert(Scancode::Num2, "pattern2".to_string());
        keys.insert(Scancode::Num3, "pattern3".to_string());
        keys.insert(Scancode::Num4, "pattern4".to_string());
        keys.insert(Scancode::Num5, "pattern5".to_string());
        keys.insert(Scancode::Num6, "pattern6".to_string());
        keys.insert(Scancode::Num7, "pattern7".to_string());
        keys.insert(Scancode::Num8, "pattern8".to_string());
        keys.insert(Scancode::Num9, "pattern9".to_string());
        keys.insert(Scancode::Num0, "pattern10".to_string());
        Events {
            pump: pump,
            actions: HashSet::new(),
            bindings: Bindings {
                k_bind: keys,
                m_bind: mouse,
            },
            mouse_x: 0,
            mouse_y: 0,
        }
    }

    pub fn update(&mut self) {
        for event in self.pump.poll_iter() {
            match event {
                KeyDown { scancode, .. } => {
                    match self.bindings.k_bind.get(&scancode.unwrap()) {
                        Some(action) => self.actions.insert(action.clone()),
                        _ => false,
                    };
                }
                KeyUp { scancode, .. } => {
                    match self.bindings.k_bind.get(&scancode.unwrap()) {
                        Some(action) => self.actions.remove(action),
                        _ => false,
                    };
                }
                MouseButtonDown { mouse_btn, .. } => {
                    match self.bindings.m_bind.get(&mouse_btn) {
                        Some(action) => self.actions.insert(action.clone()),
                        _ => false,
                    };
                }
                MouseButtonUp { mouse_btn, .. } => {
                    match self.bindings.m_bind.get(&mouse_btn) {
                        Some(action) => self.actions.remove(action),
                        _ => false,
                    };
                }
                MouseMotion { x, y, .. } => {
                    self.mouse_x = x;
                    self.mouse_y = y;
                }
                Event::Quit { .. } => {
                    self.actions.insert("quit".to_string());
                }
                _ => {}
            }
        }

    }
}
