# TODO

[] Main Menu
[] Game Menu
[] Pattern load/save
[] *In progress* - Bind key to Pattern
[] *or* implement a drop-down pattern selection menu
[] Generation tracking
[] Grid size + zoom levels, independant of resolution/window size

### Menu system

**Main Menu**. Show on application start. Will require new `states` for the application. When the simulation is running, entering **Game Menu** should pause the sim.

- main menu
    - new grid
    - new pattern
- options
    - video
        - window size (resets simulation state and redraws)
        - fullscreen
    - grid
        - empty colour
        - alive colour
        - dead colour
        - new colour
        - cell size (resets simulation state and redraws)
        - cell spacing (resets simulation state and redraws)
    - save/cancel

* will need to remove the save on exit function.

### Patterns

**Load/Save** hould be easy enough; reduce extents of pattern to 0, serialize to a vector then output as a file/toml. Loading should be functionally similar.

**Pattern drawing mode** should start in a *paused* simulation, whereupon exiting pause saves a temporary vector as first gen. Re-pausing should set the simulation back to first gen for editing.

**Bind key to Pattern**, may be a bit harder given how the key-event system currently works. Will need to rethink how events are implemented.

### Generation tracking

Maybe? How much data would it use? size of average HashMap * generation count...
Probably not much, but would pay to have memory limits in place.
